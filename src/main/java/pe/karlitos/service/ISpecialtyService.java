package pe.karlitos.service;

import pe.karlitos.model.domain.Specialty;

public interface ISpecialtyService extends ICrudService<Specialty> {

}
