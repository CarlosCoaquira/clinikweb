package pe.karlitos.service;

import pe.karlitos.model.domain.Patient;

public interface iPatientService extends ICrudService<Patient> {

}
