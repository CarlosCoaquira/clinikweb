package pe.karlitos.service;

import pe.karlitos.model.domain.Doctor;

public interface IDoctorService extends ICrudService<Doctor> {

}
