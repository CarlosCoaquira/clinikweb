package pe.karlitos.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import pe.karlitos.model.domain.Patient;
import pe.karlitos.model.repository.IPatientRepository;
import pe.karlitos.service.iPatientService;

@Service
public class PatientService implements iPatientService {

	@Autowired
	private IPatientRepository patientRepository;
	
	@Override
	public List<Patient> getAll() throws Exception {
		return patientRepository.findAll();
	}

	@Override
	public Page<Patient> getAll(Pageable pageable) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Patient saveOrUpdate(Patient entity) throws Exception {
		return patientRepository.save(entity);
	}

	@Override
	public Optional<Patient> getOne(Long id) throws Exception {
		return patientRepository.findById(id);
	}

	@Override
	public void deleteById(Long id) throws Exception {
		patientRepository.deleteById(id);

	}

}
