package pe.karlitos.service;

import java.util.List;

import pe.karlitos.model.domain.MedicalConsultation;

public interface IMedicalConsultationService extends ICrudService<MedicalConsultation> {

	List<MedicalConsultation>findMedicalConsultationtByPatientId(Long patientId);
	
}
