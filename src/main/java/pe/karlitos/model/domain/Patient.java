package pe.karlitos.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "patients")
public class Patient {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "Ingrese Nombre")
	@Column(name = "first_name", nullable = false, length = 75)
	private String firstName;

	@NotEmpty(message = "Ingrese Apellido")
	@Column(name = "last_name", nullable = false, length = 75)
	private String lastName;

	@Size(min = 8, max = 8)
	@NotEmpty(message = "Ingrese Dni")
	@Column(name = "dni", nullable = false, length = 8)
	private String dni;
	
	@NotEmpty(message = "Ingrese Número de historia Clínica")
	@Column(name = "number_history", nullable = false, length = 25)
	private String numberHistory;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNumberHistory() {
		return numberHistory;
	}

	public void setNumberHistory(String numberHistory) {
		this.numberHistory = numberHistory;
	}

	@Override
	public String toString() {
		return "Patient [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", dni=" + dni
				+ ", numberHistory=" + numberHistory + "]";
	}
	
}
