package pe.karlitos.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "doctors")
public class Doctor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "Ingrese Nombre")
	@Column(name = "first_name", nullable = false, length = 75)
	private String firstName;

	@NotEmpty(message = "Ingrese Apellido")
	@Column(name = "last_name", nullable = false, length = 75)
	private String lastName;

	@Size(min = 8, max = 8)
	@NotEmpty(message = "Ingrese Dni")
	@Column(name = "dni", nullable = false, length = 8)
	private String dni;
	
	@NotEmpty(message = "Ingrese Número de CMP")
	@Column(name = "cmp", nullable = false, length = 10)
	private String cmp;
	
	@NotNull // para objetos
	@ManyToOne
	@JoinColumn(name = "specialty_id")
	private Specialty specialty;
}
