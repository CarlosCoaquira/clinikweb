package pe.karlitos.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "details_consultation")
public class DetailConsultation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty(message = "Ingrese Diagnóstico")
	@Column(name = "diagnostic", nullable = false, length = 100)
	private String diagnostic;
	
	@NotEmpty(message = "Ingrese Tratamiento")
	@Column(name = "treatment", nullable = false, length = 100)
	private String treatment;
	
	@NotNull // para objetos
	@ManyToOne
	@JoinColumn(name = "medical_consultation_id")
	private Specialty medicalConsultation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDiagnostic() {
		return diagnostic;
	}

	public void setDiagnostic(String diagnostic) {
		this.diagnostic = diagnostic;
	}

	public String getTreatment() {
		return treatment;
	}

	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}

	public Specialty getMedicalConsultation() {
		return medicalConsultation;
	}

	public void setMedicalConsultation(Specialty medicalConsultation) {
		this.medicalConsultation = medicalConsultation;
	}
	
	
}
