package pe.karlitos.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.karlitos.model.domain.Doctor;

@Repository
public interface IDoctorRepository extends JpaRepository<Doctor, Long> {

}
