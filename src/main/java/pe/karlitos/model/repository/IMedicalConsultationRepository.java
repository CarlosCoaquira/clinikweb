package pe.karlitos.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import pe.karlitos.model.domain.MedicalConsultation;

@Repository
public interface IMedicalConsultationRepository extends JpaRepository<MedicalConsultation, Long> {

	@Query(value = "SELECT * FROM medical_consultations c WHERE c.patient_id=?1",nativeQuery = true)
	List<MedicalConsultation>findMedicalConsultationtByPatientId(Long patientId);
	
}
