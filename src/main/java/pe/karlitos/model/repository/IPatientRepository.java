package pe.karlitos.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.karlitos.model.domain.Patient;

@Repository
public interface IPatientRepository extends JpaRepository<Patient, Long>{

}
