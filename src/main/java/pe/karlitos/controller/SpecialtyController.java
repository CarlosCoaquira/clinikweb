package pe.karlitos.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import pe.karlitos.model.domain.Specialty;
import pe.karlitos.service.ISpecialtyService;

@Controller
@RequestMapping("/specialties")
public class SpecialtyController {

	@Autowired
	private ISpecialtyService specialtyService;
	
	@GetMapping
	public String listSpecialty(Model model) {
		
		try {
			List<Specialty> specialities = new ArrayList<>();
			specialities=specialtyService.getAll();
			model.addAttribute("vspecialties", specialities);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "specialty";
	}
}
