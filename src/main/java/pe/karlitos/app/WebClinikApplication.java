package pe.karlitos.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebClinikApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebClinikApplication.class, args);
		
		System.out.println("Inicio");
	}

}
